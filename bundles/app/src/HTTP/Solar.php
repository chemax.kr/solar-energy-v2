<?php

namespace Project\App\HTTP;

use PHPixie\Auth\Exception;
use PHPixie\HTTP\Request;
use DateTime;

/**
 * Simple greeting web page
 */
class Solar extends Processor
{
    /**
     * Default action
     * @param Request $request HTTP request
     * @return mixed
     */
    public function defaultAction($request)
    {

        $template = $this->components()->template();
        $container = $template->get('app:solar');
        $container->message = "Выберите интересующий вас участок";
        $container->select = $this->components()->orm()->query('serialno')->find();
        return $container;
    }

    public function selectAction($request)
    {

    }

    public function settingsAction($request)
    {

    }

    public function oldviewAction($request)
    {
        $orm = $this->components()->orm();
        $id = $request->attributes()->get('id');
        $places = $orm->query('serialno')->find();
        $columns = $this->builder->components()->database()->get()->listColumns('workDatas');
        $template = $this->components()->template();
        $container = $template->get('app:oldview');
        $start = $request->attributes()->get('start');
        if (!$start) {
            $objDateTime = new DateTime('NOW');
            $objDateTime->modify('-1 day');
            $start = $objDateTime->format('Y-m-d');
        }
        $end = $request->attributes()->get('end');
        if (!$end) {
            $objDateTime = new DateTime('NOW');
            $end = $objDateTime->format('Y-m-d');
        }

        $container->columns = $columns;
        $container->places = $places;
        $container->start = $start;
        $container->end = $end;
        $container->id = $id;
        return $container;
    }

    public function getJSONAction($request)
    {
        $orm = $this->components()->orm();
        $format = 'Y-m-d';
        $id = $request->attributes()->get('id');
        $startStr = str_replace("T", " ", $request->attributes()->get('start'));
        $endStr = str_replace("T", " ", $request->attributes()->get('end'));
        $start = DateTime::createFromFormat($format, $startStr);
        $end = DateTime::createFromFormat($format, $endStr);
        $column = $request->attributes()->get('val');
        $container['data'] = "";
        $container['label'] = "";
        $data = "";
        if ($column == "denergy") {
            $denergy = $orm->query('denergy')
                ->where('TRANDATE', '>', $start->format('Y-m-d 00:00:00'))
                ->and('TRANDATE', '<', $end->format('Y-m-d 23:59:59'))
                ->and('SERIALNO', $id)
                ->find([], ['TRANDATE', 'ENERGY'])
                ->asArray(true, 'TRANDATE');
            foreach ($denergy as $d) {
                $data[] = $d->ENERGY;
            }
            $container['data'] = $data;
            $labels = array_keys($denergy);
            foreach ($labels as $label)
            {
                $labelsWithout0[] = str_replace(" 00:00:00", " ", $label);
            }
            $container['labels'] = $labelsWithout0;


        }

        elseif ($column == "menergy") {
            $menergy = $orm->query('menergy')
                ->where('SERIALNO', $id)
                ->orderBy('YEARMONTH', 'asc')
                ->find([], ['YEARMONTH', 'ENERGY'])
                ->asArray(true, 'YEARMONTH');
            foreach ($menergy as $d) {
                $data[] = $d->ENERGY;
            }

            $labels = array_keys($menergy);
            $labelsWithout0 = "";
            $mountArray=['нулябрь','январь','февраль','март','апрель','май','июнь','июль','август','сентябрь','октябрь','ноябрь','декабрь'];
            foreach ($labels as $label)
            {
                $tmpYear = preg_replace("~..$~", " ", $label);
                $tmpLabel = preg_replace("~....~", " ", $label);
                $labelsWithout0[] = str_replace($tmpLabel, $mountArray[(int)$tmpLabel], $tmpLabel)." ".$tmpYear;
            }


            $container['data'] = $data;
            $container['labels'] =$labelsWithout0;
                //$labelsWithout0;
                //array_keys($menergy);

        }

        elseif ($column == "henergy") {
            $henergy = $orm->query('henergy')
                ->where('SERIALNO', $id)
                ->and('TRANDATE', '>', $start->format('Y-m-d'))
                ->and('TRANDATE', '<', $end->format('Y-m-d'))
                ->find([], ['TRANDATE','IHOUR' ,'ENERGY']);
            foreach ($henergy as $d) {
                $data[] = $d->ENERGY;
                $labels[] =  str_replace("00:00:00"," ".$d->IHOUR,$d->TRANDATE);
            }
            $container['data'] = $data;
            $container['labels'] = $labels;
        } else {
            $colArr = explode(",", $column);
            $colArr[] = 'CURRENTTIME';
            $workDatas = ($orm->query('workData')
                ->where('CURRENTTIME', '>', $start->format('Y-m-d 00:00:00'))
                ->and('CURRENTTIME', '<', $end->format('Y-m-d 23:59:59'))
                ->and('SERIALNO', $id)
                ->find([], [$column, 'CURRENTTIME'])->asArray(true, 'CURRENTTIME'));
            $data = "";
            foreach ($workDatas as $d) {
                $data[] = $d->$column;
            }
            $container['data'] = $data;
            $container['labels'] = array_keys($workDatas);

        }
        return json_encode($container);
    }

    public function viewAction($request)
    {
        $orm = $this->components()->orm();
        $id = $request->attributes()->get('id');
        $place = $orm->query('serialno')->where('SERIALNO', $id)->findOne();
        $columns = $this->builder->components()->database()->get()->listColumns('workDatas');

//        $countD = $orm->query('denergy')->where('SERIALNO', $id)->count();
//        $countH = $orm->query('henergy')->where('SERIALNO', $id)->count();
//        $countM = $orm->query('menergy')->where('SERIALNO', $id)->count();


//        if ($countD > 0) {
//            $columnsDenergy = $this->builder->components()->database()->get()->listColumns('denergy');
//        }
//        if ($countH > 0) {
//            $columnsHenergy = $this->builder->components()->database()->get()->listColumns('henergy');
//        }
//        if ($countM > 0) {
//            $columnsMenergy = $this->builder->components()->database()->get()->listColumns('menergy');
//        }


        $template = $this->components()->template();
        $container = $template->get('app:view');
        $start = $request->attributes()->get('start');
        if (!$start) {
            $objDateTime = new DateTime('NOW');
            $objDateTime->modify('-1 day');
            $start = $objDateTime->format('Y-m-d');
        }
        $end = $request->attributes()->get('end');
        if (!$end) {
            $objDateTime = new DateTime('NOW');
            $end = $objDateTime->format('Y-m-d');
        }

        $container->columns = $columns;
        $container->place = $place;
        $container->start = $start;
        $container->end = $end;
        $container->id = $id;
        return $container;
    }

    public function getdataAction($request)
    {
        $orm = $this->components()->orm();
//        $regexSqlTableHeader = '#, "(.*?)" |\("(.*?)"#';
        $regexSqlTableHeader = '/"([^"]+)"\s+(?:VARCHAR|INTEGER|timestamp|double|smallint)/im';
        $tochka = $request->attributes()->get('id');
//        $dir = "karagan";
        $path = "/opt/tochki";
        $bin = "/var/www/solars/bin";
        $dataPath = "/tmp/tochki";
        $dirArr = scandir($path);
        unset($dirArr[0]);
        unset($dirArr[1]);
        print_r($dirArr);
        foreach ($dirArr as $dir) {
            echo "<pre>" . $dir . "<br />";
            $tochka = $dir;
            $execString = "$bin/exportData.sh $tochka $path $bin";
            print_r(exec($execString));
            $fileContent = file_get_contents($dataPath . "/data/SERIALNO.csv");
            print_r($fileContent);
            $arr = explode(",", $fileContent);
//            print_r($arr);
            $schema = file_get_contents($dataPath . "/data/work_data.sql");
            preg_match_all($regexSqlTableHeader, $schema, $schemaArr);
//            print_r($schemaArr);
            $serial = $orm->query('serialno')->where('SERIALNO', str_replace("\"", "", $arr[2]))->findOne();
            if (!$serial) {
                $serialRepo = $orm->repository('serialno');
                $PRODID = str_replace("\"", "", $arr[1]);
                $SERIALNO = str_replace("\"", "", $arr[2]);
                $MPPTNUMBER = str_replace("\"", "", $arr[3]);
                $PARALLEL = str_replace("\"", "", $arr[4]);
                $MODETYPE = "00";
                if (isset($arr[5])) {
                    $MODETYPE = str_replace("\"", "", $arr[5]);
                }
                $serial = $serialRepo->create([
                    'PRODID' => $PRODID,
                    'SERIALNO' => $SERIALNO,
                    'MPPTNUMBER' => $MPPTNUMBER,
                    'PARALLEL' => $PARALLEL,
                    'MODETYPE' => $MODETYPE,
                    'NAME' => $dir
                ]);
                $serial->save();
            }

            $orm->query('workData')->where('SERIALNO', $serial->SERIALNO)->delete();
            $database = $this->components()->database();
            $sql = $this->import_csv("workDatas", $schemaArr[1], '/tmp/tochki/data/WORK_DATA.csv', ",", '"', '\\', '\\n');
            print_r($sql);
            $database->get('default')->execute($sql);
            $regexForGlob = $dataPath . '/data/*.csv';
            $dirArr = glob($regexForGlob);
            print_r($dirArr);
            echo "<br />";
            foreach ($dirArr as $dir) {

                print_r($dir);
                $table = strtolower(str_replace($dataPath . '/data/', "", $dir));
                $table = str_replace('.csv', "", $table);
                $schemaTableFile = str_replace('.csv', '.sql', strtolower($dir));
                $schemaTableString = file_get_contents($schemaTableFile);
//                print_r($schemaTableString);
                preg_match_all($regexSqlTableHeader, $schemaTableString, $schemaTableArr);
                if (($table != "serialno") && ($table != "work_data")) {
                    if (mb_substr($table, 1) == "energy") {
                        $table = rtrim($table, "y") . "ie";
                        echo $table;
                    }

                    $sql = $this->import_csv($table . "s", $schemaTableArr[1], $dir, ",", '"', '\\', '\\n');
                    echo "<br>=============<br>";
                    print_r($sql);
                    echo "<br>=============<br>";
//                    try {
                    $orm->query($table)->where('SERIALNO', $serial->SERIALNO)->delete();
                    $database->get('default')->execute($sql);
//                    } catch (Exception $e) {
//                        print_r($e);
//                    }

                }

            }

        }

        return "";

        /* *) проверяем дату изменения базы
         * *) если дата не соответствует предыдущей - делаем экспорт данных
         *      CALL SYSCS_UTIL.SYSCS_EXPORT_TABLE_LOBS_TO_EXTFILE(null,'WORK_DATA','/tmp/staff.csv',',','"','UTF-8', '/tmp/dat.dat');
         * CALL SYSCS_UTIL.SYSCS_EXPORT_TABLE_LOBS_TO_EXTFILE(null,'WORK_DATA','/tmp/tochki/data/WORK_DATA.csv',',','"','UTF-8', '/tmp/tochki/data/WORK_DATA.dat');
         * *) дропнуть и пересоздать таблицу
         * *) Загружаем эти данные как csv во временную базу mysql
         *      TRUNCATE TABLE WORK_DATA;
         *      LOAD DATA INFILE '/tmp/tochki/data/WORK_DATA.csv' INTO TABLE workDatas FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n';

         * */
    }

    protected function myScanDir($path, $mask)
    {

        $tempArr = scandir($path);
        $rArr = false;
        foreach ($tempArr as $item) {
            if (preg_match($mask, $item) > 0) {
                $rArr[] = $item;
            }
        }
        return $rArr;
    }

    protected function import_csv(
        $table,        // Имя таблицы для импорта
        $afields,        // Массив строк - имен полей таблицы
        $filename,        // Имя CSV файла, откуда берется информация
        // (Абсолютный путь)
        $delim = ',',        // Разделитель полей в CSV файле
        $enclosed = '"',    // Кавычки для содержимого полей
        $escaped = '\\',        // Ставится перед специальными символами
        $lineend = '\\n',    // Чем заканчивается строка в файле CSV
        $hasheader = FALSE)
    {    // Пропускать ли заголовок CSV

        if ($hasheader) $ignore = "IGNORE 1 LINES ";
        else $ignore = "";
        $q_import =
            "LOAD DATA INFILE '" .
            $filename . "' INTO TABLE " . $table . " " .
            "FIELDS TERMINATED BY '" . $delim . "' ENCLOSED BY '" . $enclosed . "' " .
//            "    ESCAPED BY '".$escaped."' ".
            "LINES TERMINATED BY '" . $lineend . "' " .
            $ignore .
            "(" . implode(',', $afields) . ")";
        return $q_import;
    }
}