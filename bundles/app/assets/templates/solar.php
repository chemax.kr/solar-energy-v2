<?php $this->layout('app:layout'); ?>

<h2><?= $_($message); ?></h2>
<ul>
    <?php
    foreach ($select as $tochka) {
        $url = $this->httpPath('app.action', ['processor' => 'solar', 'action' => 'view', 'id' => $tochka->SERIALNO]);
        ?>
        <li><a href="<?= $url ?>"><?= $tochka->NAME ?></a></li>
        <?php
    }
    ?>
</ul>
