<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 06.03.2017
 * Time: 14:17
 */
$this->layout('app:layout'); ?>
<?php ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h3>
                <?=$place->NAME?>
            </h3>

            <label>От: </label><input id="start" name="start" type="date" value="<?=$start?>">
            <label> До: </label><input id="end" name="end" type="date" value="<?=$end?>">
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <ul>
                <?php
                foreach ($columns as $key => $value) {
                    echo "<h6><li><a value='$value' name='$value' onclick='getData(this)'>$value</a></li></h6>";
                } ?>

            </ul>
        </div>
        <div class="col-md-10">
            <div id="charts">

            </div>

        </div>
    </div>
</div>

<script>
    var getData = function (element) {
        console.log(element.name);
        var start = document.getElementById("start").value;
        var end = document.getElementById("end").value;
        console.log(start);
        console.log(end);
        $('#charts').append('<canvas id="'+ element.name +'" width="400" height="100"></canvas>');
        url = "/solar/getJSON/<?=$id?>/"+ start +"/"+ end +"/"+element.name;
        $.get( url, function( data ) {
            data = JSON.parse(data);
            console.log(data.data);
            newChart(element.name, data.labels, element.name, data.data);
        });

    }
</script>
<script>
    var newChart = function (elemId, labels, label, data) {
        var ctx = document.getElementById(elemId);
        var pvV = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: label,
                    data: data,
                    backgroundColor: "rgba(75,192,192,1)",
                    borderColor: "rgba(0,0,192,1)",
                    pointBorderColor: "rgba(75,0,0,1)",
                    pointHoverRadius: 10,
                    pointRadius: 2,
                    borderWidth: 1
                }]
            },
            options: {

                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

</script>