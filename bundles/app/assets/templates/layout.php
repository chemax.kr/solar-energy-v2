<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all"
          rel="stylesheet">
    <title>Солнечная энергия</title>
    <link rel="stylesheet" href="/bundles/app/css/bootstrap.css">
    <script src="/bundles/app/js/jquery.js"></script>
    <script src="/bundles/app/js/bootstrap.js"></script>
    <script src="/bundles/app/js/dataTables.bootstrap.min.js"></script>
    <script src="/bundles/app/js/jquery.dataTables.min.js"></script>
    <script src="/bundles/app/css/Chart.bundle.min.js"></script>
    <link rel="stylesheet" href="/bundles/app/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/bundles/app/css/dataTables.bootstrap.min.css">
</head>
<body>
<h1>Solars</h1>
<?php $this->childContent(); ?>
</body>
</html>
