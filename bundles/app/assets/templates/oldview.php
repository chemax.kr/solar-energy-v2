<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 09.03.2017
 * Time: 10:46
 */
$this->layout('app:layout'); ?>

<h2>Настройки фильтра</h2>
Интервал:
<p>От <input type="date" name="start" id="start" value="<?= $start ?>">
    До <input type="date" name="end" id="end" value="<?= $end ?>">
    Выбор контроллера: <select name="controller_id" id="controller_id">
        <?php foreach ($places as $place) { ?>
            <option value="<?= $place->SERIALNO ?>"><?= $place->NAME ?></option>
            <?php
        }
        ?>
    </select>
    Выбор значения: <select name="valueSrc" id="valueSrc">
        <option value="PVINPUTVOLTAGE1">Входящий вольтаж</option>
        <option value="henergy">kwh по часам</option>
        <option value="denergy">kwh по дням</option>
        <option value="menergy">kwh по месяцам</option>

    </select>
    <a class="btn btn-sm btn-danger" onclick='getData(this)'>Применить</a>
</p>
Графики с одинаковым(к примеру "karagan: PVINPUTVOLTAGE1") лэйблом перезаписываются.
<div id="charts">

</div>

<script>
    var getData = function (element) {
        console.log(element.name);
        var start = document.getElementById("start").value;
        var end = document.getElementById("end").value;
        var serial = document.getElementById("controller_id").value;
        var placeName = $('#controller_id option:selected').text();
//        var tables = ["PVINPUTVOLTAGE1", "denergy"];
        var item = document.getElementById("valueSrc").value;
        console.log(start);
        console.log(end);
        console.log(serial);
//            document.getElementById(item + serial).remove();
        $('#' + item + serial).remove();
        $('#rm' + item + serial).remove();
        $('#charts').append('<a class="btn pull-right btn-danger" id=' + "rm"+ item + serial +' onclick="removeCanvas(this)">Удалить [' + placeName + ': '+ item + ']</a>');
        $('#charts').append('<canvas id=' + item + serial + ' width="400" height="100"></canvas>');

        url = "/solar/getJSON/" + serial + "/" + start + "/" + end + "/" + item;
        console.log(url);
        $.get(url, function (data) {
            data = JSON.parse(data);
            console.log(data.data);
            if (item == "menergy" || item == "denergy")
            {
                type = "bar";
            }
            else
            {
                type = "line";
            }
            newLineChart(item + serial, data.labels, placeName + ": " + item, data.data, type);

        });

//        $('#charts').append('<canvas id="'+ element.name +'" width="400" height="100"></canvas>');
//        url = "/solar/getJSON/"+ serial +"/"+ start +"/"+ end +"/"+element.name;
//        $.get( url, function( data ) {
//            data = JSON.parse(data);
//            console.log(data.data);
//            newLineChart(element.name, data.labels, element.name, data.data);
//        });


    }
</script>
<script>
    var removeCanvas = function (e) {
        console.log(e.id);
        $('#' + e.id.replace(/rm/,'')).remove();
        $('#' + e.id).remove();
//        $('#rm' + item + serial).remove();
    };
</script>
<script>
    var newLineChart = function (elemId, labels, label, data, type = "line") {
        var ctx = document.getElementById(elemId);
        var chart = new Chart(ctx, {
            type: type,
            data: {
                labels: labels,
                datasets: [{
                    label: label,
                    data: data,
                    backgroundColor: "rgba(75,192,192,1)",
                    borderColor: "rgba(0,0,192,1)",
                    pointBorderColor: "rgba(75,0,0,1)",
                    pointHoverRadius: 10,
                    pointRadius: 2,
                    borderWidth: 1
                }]
            },
            options: {

                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    };
    
</script>