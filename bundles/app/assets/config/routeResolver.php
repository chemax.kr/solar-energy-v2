<?php

return array(
    'type'      => 'group',
    'defaults'  => array('action' => 'default'),
    'resolvers' => array(
        
        'action' => array(
            'path' => '<processor>/<action>(/<id>)'
        ),
        'timeshift' => array(
            'path' => '<processor>/<action>(/<id>/<start>/<end>/<val>)'
        ),
        'frontpage' => array(
            'path' => '',
            'defaults' => ['processor' => 'solar', 'action' => 'oldview']
        ),
        'processor' => array(
            'path'     => '(<processor>)',
            'defaults' => array('processor' => 'solar')
        ),

    )
);
