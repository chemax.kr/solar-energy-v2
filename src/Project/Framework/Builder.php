<?php

namespace Project\Framework;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
/**
 * Your projects main factory, usually referenced as $frameworkBuilder.
 *
 * You can use it to override and customize the framework.
 */
class Builder extends \PHPixie\BundleFramework\Builder
{
    /**
     * Your Bundles registry
     * @return Bundles
     */
    protected function buildBundles()
    {
        return new Bundles($this);
    }

    /**
     * Your extension registry registry
     * @return Extensions
     */
    protected function buildExtensions()
    {
        return new Extensions($this);
    }

    /**
     * Projects root directory
     * @return string
     */
    protected function getRootDirectory()
    {
        return realpath(__DIR__.'/../../../');
    }
    protected function buildLogger()
    {
        $log = new Logger('solars');
        $log->pushHandler(new StreamHandler('/tmp/solars.log', Logger::DEBUG));

        return $log;
    }
}