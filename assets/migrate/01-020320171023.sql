CREATE TABLE `workDatas` (
  `WORKID` int(11) NOT NULL,
  `PRODID` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `SERIALNO` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `WORKMODE` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `GRIDVOLTAGER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `GRIDPOWERR` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `GRIDCURRENTR` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `GRIDFREQUENCY` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTVOLTAGER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTPOWERR` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTFREQUENCY` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTCURRENTR` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `OUTPUTLOADPERCENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PBATTERYVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `NBATTERYVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `BATTERYCAPACITY` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `BATTERYPIECENUMBER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `BATTERYTOTALCAPACITY` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `BATTERYREMAINTIME` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `CHARGINGCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTPOWER1` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTPOWER2` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTPOWER3` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTVOLTAGE1` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTVOLTAGE2` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTVOLTAGE3` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `INNERTEMPERATURE` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `MAXTEMPERATURE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RGRIDVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SGRIDVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TGRIDVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RSGRIDVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RTGRIDVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `STGRIDVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RGRIDCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SGRIDCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TGRIDCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RPHASEPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SPHASEPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TPHASEPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `WHOLEPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RPHASEACOUTPUTVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SPHASEACOUTPUTVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TPHASEACOUTPUTVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RSPHASEACOUTPUTVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RTPHASEACOUTPUTVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `STPHASEACOUTPUTVOLTAGE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RACOUTPUTCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SACOUTPUTCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TACOUTPUTCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `RPHASEACOUTPUTLOAD` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SPHASEACOUTPUTLOAD` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TPHASEACOUTPUTLOAD` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `WHOLEACOUTPUTLOAD` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `OUTPUTAPPARENTPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `OUTPUTACTIVEPOWER` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `CURRENTTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `WHOLEGRIDOUTPUTPOWER` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `WHOLEGRIDOUTPUTAPPERENTPOWER` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `GRIDOUTPUTPOWERPERCENTAGE` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTACTIVEPOWERR` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTACTIVEPOWERS` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTACTIVEPOWERT` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTAPPERENTPOWERR` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTAPPERENTPOWERS` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `ACOUTPUTAPPERENTPOWERT` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `TTLCHARGINGCURRENT` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `TTLOUTPUTAPPARENTPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TTLOUTPUTACTIVEPOWER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `TTLOUTPUTPERCENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `BATDISCURRENT` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTCURRENTFORBATTTERY` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTCURRENT1` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTCURRENT2` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `PVINPUTCURRENT3` varchar(16) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- statement

CREATE TABLE `serialnos` (
  `SID` int(11) NOT NULL,
  `PRODID` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `SERIALNO` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `MPPTNUMBER` int(11) DEFAULT NULL,
  `PARALLEL` int(11) DEFAULT NULL,
  `MODETYPE` varchar(4) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `serialnos`
  ADD PRIMARY KEY (`SID`);

ALTER TABLE `serialnos`
  MODIFY `SID` int(11) NOT NULL AUTO_INCREMENT;
-- statement
CREATE TABLE `denergys` (
  `DID` int(11) NOT NULL,
  `PRODID` varchar(20) NOT NULL,
  `SERIALNO` varchar(64) NOT NULL,
  `TRANDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ENERGY` double NOT NULL,
  `ISCOMPLETE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `henergys` (
  `HID` int(11) NOT NULL,
  `PRODID` varchar(20) NOT NULL,
  `SERIALNO` varchar(64) NOT NULL,
  `TRANDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IHOUR` int(11) NOT NULL,
  `ENERGY` double NOT NULL,
  `ISCOMPLETE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `menergys` (
  `MID` int(11) NOT NULL,
  `PRODID` varchar(20) NOT NULL,
  `SERIALNO` varchar(64) NOT NULL,
  `YEARMONTH` varchar(64) NOT NULL,
  `ENERGY` double NOT NULL,
  `ISCOMPLETE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `yenergys` (
  `YID` int(11) NOT NULL,
  `PRODID` varchar(20) NOT NULL,
  `SERIALNO` varchar(64) NOT NULL,
  `IYEAR` int(11) NOT NULL,
  `ENERGY` double NOT NULL,
  `ISCOMPLETE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;