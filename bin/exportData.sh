#!/bin/bash
tochka=$1
archivePath=$2
bin="/var/www/solars/bin"
srcBase="/tmp/tochki/datas"
data="/tmp/tochki/data"
rm $srcBase/ -rf
rm $data/ -rf
mkdir -p $srcBase
mkdir -p $data
cp $archivePath/$tochka/* $srcBase/ -R
#ls -la $srcBase
for file in $bin/sql/*.sql
do
/usr/bin/ij < $file
done
#/usr/bin/ij < $bin/sql/export.sql
#/usr/bin/ij < $bin/sql/serial.sql
/usr/bin/dblook -d "jdbc:derby:$srcBase" -o $data/work_data.sql -t WORK_DATA
/usr/bin/dblook -d "jdbc:derby:$srcBase" -o $data/henergy.sql -t HENERGY
/usr/bin/dblook -d "jdbc:derby:$srcBase" -o $data/denergy.sql -t DENERGY
/usr/bin/dblook -d "jdbc:derby:$srcBase" -o $data/menergy.sql -t MENERGY
/usr/bin/dblook -d "jdbc:derby:$srcBase" -o $data/yenergy.sql -t YENERGY
/usr/bin/dblook -d "jdbc:derby:$srcBase" -o $data/serialno.sql -t SERIALNO
#cat $data/SERIALNO.csv